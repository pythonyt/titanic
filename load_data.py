import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler

TRAIN_DATA = 'input/train.csv'
TEST_DATA = 'input/test.csv'

# csv読込
# trainとtest両方に適用したい処理を加える


def read_csv(path):
    df = pd.read_csv(path)

    # Embarkedの欠損値をSで補完
    df['Embarked'] = df['Embarked'].fillna('S')

    # Fareの欠損値を中央値で補完
    df['Fare'] = df['Fare'].fillna(df['Fare'].median())

    # Ageの欠損値を平均値で補完
    # df['Age'] = df['Age'].fillna(df['Age'].mean())

    # 年齢の平均
    age_avg = df['Age'].mean()
    # 年齢の標準偏差
    age_std = df['Age'].std()
    age_null_count = df['Age'].isnull().sum()
    # Ageの欠損値を平均と標準偏差から乱数を生成し補完
    age_null_random_list = np.random.randint(
        age_avg - age_std, age_avg + age_std, size=age_null_count)
    df['Age'][np.isnan(df['Age'])] = age_null_random_list
    df['Age'] = df['Age'].astype(int)

    # 家族構成
    # df['FamilySize'] = df['SibSp'] + df['Parch'] + 1
    # df[['Survived', 'FamilySize']].groupby(
    #     'FamilySize', as_index=False).mean().sort_values(by='Survived', ascending=False)

    # 文字を整数へパース
    df['Sex'] = df['Sex'].map({'female': 0, 'male': 1}).astype(int)
    df['Embarked'] = df['Embarked'].map({'S': 0, 'C': 1, 'Q': 2}).astype(int)

    # 標準化
    std_scale = StandardScaler().fit(df[['Age', 'Fare']])
    df[['Age', 'Fare']] = std_scale.transform(df[['Age', 'Fare']])

    return df


def load_train_data():
    df = read_csv(TRAIN_DATA)
    df = df.drop(['PassengerId', 'Name', 'Ticket', 'Cabin'], axis=1)
    return df


def load_test_data():
    df = read_csv(TEST_DATA)
    df = df.drop(['Name', 'Ticket', 'Cabin'], axis=1)
    return df


if __name__ == '__main__':
    print(load_train_data)
    print(load_test_data)
