import pandas as pd
import numpy as np
from logging import StreamHandler, DEBUG, Formatter, FileHandler, getLogger
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import cross_val_score
from sklearn.metrics import log_loss, roc_auc_score, roc_curve, accuracy_score
from tqdm import tqdm

from load_data import load_train_data, load_test_data


# 欠損値表示
def null_table(df):
    null_val = df.isnull().sum()
    per = 100 * df.isnull().sum() / len(df)
    null_table = pd.concat([null_val, per], axis=1)
    null_table_re = null_table.rename(columns={0: '欠損値', 1: '%'})
    return null_table_re


def cross_validate(clf, x_train, y_train):
    cv = StratifiedKFold(n_splits=n, random_state=seed, shuffle=True)

    logloss_scores = []
    auc_scores = []
    acc_scores = []

    # テストデータを分割し、順次処理
    for train_idx, val_idx in cv.split(x_train, y_train):
        trn_x = x_train.iloc[train_idx, :]
        val_x = x_train.iloc[val_idx, :]
        trn_y = y_train[train_idx]
        val_y = y_train[val_idx]

        # 予測モデルの構築
        clf.fit(trn_x, trn_y)

        # 予測モデルの精度評価を検証データで行う。
        pred = clf.predict_proba(val_x)[:, 1]
        sc_logloss = log_loss(val_y, pred)
        sc_auc = roc_auc_score(val_y, pred)
        sc_acc = clf.score(val_x, val_y)

        logloss_scores.append(sc_logloss)
        auc_scores.append(sc_auc)
        acc_scores.append(sc_acc)

        logger.debug(f'logloss: {sc_logloss}, auc: {sc_auc}, acc: {sc_acc}')

    # logger.info(
    #     f'logloss_mean: {np.mean(logloss_scores)}, auc_mean: {np.mean(auc_scores)}, acc_mean: {np.mean(acc_scores)}')

    return np.mean(logloss_scores), np.mean(auc_scores), np.mean(acc_scores)


logger = getLogger(__name__)

DIR = 'result/'
SAMPLE_SUBMIT_FILE = 'input/gender_submission.csv'

# random seed
seed = 0
# 交差検証の回数
n = 5

if __name__ == '__main__':

    # log設定
    log_fmt = Formatter(
        '%(asctime)s %(name)s %(lineno)d [%(levelname)s][%(funcName)s] %(message)s ')
    handler = StreamHandler()
    handler.setLevel('INFO')
    handler.setFormatter(log_fmt)
    logger.addHandler(handler)

    handler = FileHandler(DIR + 'train.py.log', 'a')
    handler.setLevel(DEBUG)
    handler.setFormatter(log_fmt)
    logger.setLevel(DEBUG)
    logger.addHandler(handler)

    logger.info('start')

    # trainデータ読込
    df = load_train_data()

    # Survivedをdrop(過学習防止)
    x_train = df.drop('Survived', axis=1)
    y_train = df['Survived'].values

    # 一応columnsをとっておく
    use_cols = x_train.columns.values
    logger.info(f'train columns: {use_cols.shape} {use_cols}')

    logger.info(f'train data load end {x_train.shape}')

    # 複数のclassifier の適用
    models = []
    names = []
    list_logloss = []
    list_auc = []
    list_acc = []

    # 機械学習モデルをリストに格納
    models.append(
        ('LogisticRegression', LogisticRegression(random_state=seed)))
    models.append(('KNC', KNeighborsClassifier()))
    models.append(('DTC', DecisionTreeClassifier(random_state=seed)))
    # models.append(('SVM', SVC(random_state=seed)))
    models.append(('AdaBoost', AdaBoostClassifier(random_state=seed)))
    models.append(
        ('GradientBoosting', GradientBoostingClassifier(random_state=seed)))

    # 複数のclassifierでvalidate
    for name, model in models:

        # Cross Validation
        logloss, auc, acc = cross_validate(model, x_train, y_train)
        names.append(name)
        list_logloss.append(logloss)
        list_auc.append(auc)
        list_acc.append(acc)

    # 適用したclassifierのスコア表示
    for i in range(len(names)):
        logger.info(
            f'{names[i]} >> logloss: {list_logloss[i]}, auc: {list_auc[i]}, acc: {list_acc[i]}')

    # # ロジスティック回帰
    # clf = LogisticRegression(random_state=seed)
    # # 学習
    # clf.fit(x_train, y_train)

    logger.info('train end')

    # df = load_test_data()

    # x_test = df[use_cols]

    # logger.info(f'test data load end {x_test.shape}')
    # pred_test = clf.predict_proba(x_test)

    # df_submit = pd.read_csv(SAMPLE_SUBMIT_FILE)
    # df_submit['Survived'] = pred_test

    # df_submit.to_csv(DIR + 'submit.csv', index=False)

    # logger.info('submit end')
